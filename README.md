# Portail access - Tarte au citron js

## reCAPTCHA (pistes) // Cookie + script
- **_GRECAPTCHA**
- **rc::b** (je ne le vois que dans le tracer firefox // cache navigateur ?)
- **rc::c** (je ne le vois que dans le tracer firefox // cache navigateur ?)
- [cf7-tac-recaptcha](https://github.com/jerome-rdlv/cf7-tac-recpatcha)
- https://stackoverflow.com/questions/51554527/how-can-i-use-tarteaucitron-rgpd-epr-with-contactform7-integrated-recaptcha

## Crisp (pistes) // cookies + script
- **crisp-client%2Fsession%2Feffaf24f-721a-4dcb-bdd8-f50d84afe611**
- **crisp-client/domain-detect/1613854166761**
- https://github.com/AmauriC/tarteaucitron.js/issues/224


## Jetpack ? // Script


## WPML // Fonctionnel ?


## Google (visiblement dans le cache du navigateur)
- 1P_JAR
- _gat
- _ga
- _gid
- ANID
- CGIC
- CONSENT
- DV
- NID
- OGPC
- SNID


# Script google analytics

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-105477635-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-105477635-1');
</script>
